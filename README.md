<div><img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/header3.jpg"></div>

# Timothy Stone

Hello! :wave:

My typical prologue reads like this:

> Tim is a father, blogger, OSS committer, source control nerd, Java certified web developer, analog wargamer, home brewer, and lifelong D&D gamer.

:closed_lock_with_key: [Get my KNSL Public GPG Key](https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/knsl-public-key.asc)

## Skills

Highlights,  _always_ mastering, _always_ expanding...

<center>
<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/angular-icon.png" title="AngularJS and Angular" alt="AngularJS and Angular">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/apache-icon.png" title="Apache HTTPD" alt="Apache HTTPD">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/git-icon.png" title="Git" alt="Git">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/gitlab-icon.png" title="GitLab and GitLab CI" alt="GitLab and GitLab CI">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/hibernate-icon.png" title="Hibernate" alt="Hibernate">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/intellij-icon.png" title="IntelliJ" alt="IntelliJ">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/java-icon.png" title="Java" alt="Java">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/javascript-icon.png" title="JavaScript" alt="JavaScript"><br>
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/jhipster-icon.png" title="JHipster" alt="JHipster">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/lucene-icon.png" title="Apache Lucene" alt="Apache Lucene">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/maven-icon.png" title="Apache Maven" alt="Apache Maven">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/netbeans-icon.png" title="NetBeans" alt="NetBeans">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/node-icon.png" alt="Node and NPM">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/spring-icon.png" title="Spring and Spring Boot" alt="Spring and Spring Boot">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/tomcat-icon.png" title="Apache Tomcat" alt="Apache Tomcat">
&nbsp;<img src="https://gitlab.com/timothy.stone/timothy.stone/-/raw/master/src/zsh-icon.png" title="Shell (`zsh`)" alt="Shell (`zsh`)">
</center>

## Career

:clapper: Film/TV/Print Location Scout/Manager and Producer ([IMDB](https://www.imdb.com/name/nm0832205/))<br>
:credit_card: 15 years in Consumer Credit Card customer acquisition and servicing as a web application and solution architect<br>
🛡️ Solution architect in Excess & Surplus (E&S) insurance

## Hobbies

:beers: Homebrewing. My Untappd Home Brewery ... [Thirsty Stone](https://untappd.com/thirstystone)<br>
:dragon: D&D, WFRP 2e, and more. "Always the DM, Never the Player."<br>
